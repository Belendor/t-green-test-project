<?php
class Backend{
	
	// flag to indicate to only return result between <reponse> tags
	const BACKEND_RETURN_RESPONSE_TAGS = 'BACKEND_RETURN_RESPONSE_TAGS';
	// flag to indicitate ....
	const BACKEND_STRIP_COMMENTS       = 'BACKEND_STRIP_COMMENTS';

	protected $_hostname   = null;
	protected $_post       = null;

	protected $_connection = null;

	public function __construct( $hostname , $port ){
		
		$this->_hostname = $hostname;
		$this->_port     = $port;
		
	}

	public function __call( $name , $args ){

		$parameters = array_shift( $args );
		$options    = (array)array_shift( $args );

		$parameters = $parameters + array( 'program' => $name );

		$this->_connect( $this->_hostname );
		$this->_sendParameters( $parameters );
		$content = $this->_readAll();
		$this->_close();

		if( array_search( self::BACKEND_STRIP_COMMENTS , $options ) !== false ) {
			$content = preg_replace( '#<!--(.*?)-->\s*#s' , null , $content );

		}
		if( array_search( self::BACKEND_RETURN_RESPONSE_TAGS , $options) !== false ) {
			/*
			$count = preg_match_all( '#<response>(.*?)</response>#is' , $content , $matches );
			$tmp   = null;
			for( $i = 0 ; $i < $count ; $i++ ) {
				$tmp .= $matches[1][$i];
			}
			$content = $tmp;
			*/
			$tmp = null;
			$pos = strpos( $content , '<response>' );
			while( $pos !== false ) {
				if( false !== ( $pos2 = strpos( $content , '</response>' , $pos ) ) ) {
					$tmp .= substr( $content , $pos + 10 , $pos2 - $pos - 10 );
					$pos  = strpos( $content , '<reponse>' , $pos2 );
				}
			}
			$content = $tmp;
		}

		return $content;
	}

	protected function _connect( )
	{
		$remote = 'tcp://' . $this->_hostname . ':' . $this->_port;
		$this->_connection = @stream_socket_client( $remote , $errno , $errstr , 5 );
		if( $this->_connection === false ) {
			throw new Exception( 'Could not connect to "' . $remote . '" : (' . $errno . ') ' . $errstr );
		}
	}

	protected function _close()
	{
		fclose( $this->_connection );
	}

	protected function _readAll()
	{
		$result = null;
		while( ! feof ( $this->_connection ) ) {
			$result .= str_replace( "\x00" , " " , fgets( $this->_connection , 8192 ) );
		}
		return mb_convert_encoding($result , 'UTF-8' , 'Windows-1252' );
	}

	protected function _sendParameters( $parameters = array( ) )
	{
		$param_str = $this->_buildParameterString( $parameters );
		if( ( @fwrite( $this->_connection , $param_str ) ) === false ) {
			throw new Exception( 'Problem sending parameters to backend' );
		}
	}

	protected function _buildParameterString( $parameters = array() )
	{
		$query_string = http_build_query( $parameters , null , ' ' ) . PHP_EOL;

		return $query_string;
	}
}

?>