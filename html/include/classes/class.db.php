<?php
/*
   +---------------------------------------------------------------------------+
   |  class.db.php                                                             |
   +---------------------------------------------------------------------------+
   |  php class om te connecteren met de mysql databank                        |
   |                                                                           |
   +---------------------------------------------------------------------------+
   |  Written by Davy Van Vooren ( 19 may 2005 )                               |
   +---------------------------------------------------------------------------+
   |  Documentation                                                            |
   |                                                                           |
   +---------------------------------------------------------------------------+
   |  Aanpassingen:                                                            |
   |  + 2005-05-19 : begin development                                         |
   |  + 2005-07-25 : toevoegen van "queryArray" (stefanst)                     |
   |  + 2005-07-25 : toevoegen van "queryOneRow" (stefanst)                    |
   |  + 2006-10-30 : toevoegen van "getLastInsertID" (stefanst)                |
   +---------------------------------------------------------------------------+
*/
/*****************************************************************
	`description`
										 19/05/2005 16:58
*****************************************************************/
class Db
{
	var $_dbname = '';				// database name
	var $_dbuser = '';		// user name for database connection
	var $_dbpass = '';		// user pass for database connection
	var $_dbhost = '';		// database server host name
	var $_dbhandle;					// a mysql resource connection
	var $_dberror;

	var $qdescr;

	/*
	 * Db - constructor
	 */
	function __construct( $dbname = null, $dbuser = null, $dbpass = null, $dbhost = null, $connect = true )
	{
		require_once realpath( __DIR__ ) . '/../../vendor/autoload.php';
		$configValues = \Tui\Config::getConfig( 'applic_html' )->getValues();

		if ( $dbname == null ){
			$dbname = 'JAROS';
		}

		if ( $dbuser == null ){
			$dbuser = $configValues[ 'db.default.username' ];
		}

		if ( $dbpass == null ){
			$dbpass = $configValues[ 'db.default.password' ];
		}

		if( $dbhost == null ){
			$dbhost = $configValues[ 'db.default.server' ];
		}


		$this->_dbhost = $dbhost;
		$this->_dbuser = $dbuser;
		$this->_dbpass = $dbpass;
		$this->_dbname = $dbname;

		if ( $connect ){
			$this->connect();
		}

	}

	function bind( $query, $bind = array() )
	{
		foreach( $bind as $item => $value ) {
			if( is_array( $value ) ) {
				$query = $this->bind( $query, $value );
			} else {
				$query = str_replace( $item, $value, $query );
			}
		}

		return $query;
	}

	function connect()
	{
		$this->_dbhandle = @mysql_connect( $this->_dbhost, $this->_dbuser, $this->_dbpass, true );
		if( ! $this->_dbhandle )
		{
			$this->_dberror = mysql_error();
			die("No database connection");
		}
		$result = @mysql_select_db( $this->_dbname, $this->_dbhandle );
		if( ! $result )
		{
			$this->_dberror = mysql_error( $this->_dbhandle );
			die("No database");
		}
	}

	function query( $query )
	{
		$this->qdescr = $query;
		$result = mysql_query( $query, $this->_dbhandle );
		if( ! $result )
		{
			$this->_dberror = mysql_error( $this->_dbhandle );
		}
		return $result;
	}

	function queryArray( $query )
	{
		$this->qdescr = $query;
		$result = $this->query( $query );
		if( $result === false ) {
			return false;
		} else {
			$table = array();
			while( ( $row = mysql_fetch_assoc( $result ) ) !== false ) {
				$table[] = $row;
			}
			mysql_free_result( $result );
			return $table;
		}
	}

	function queryOneRow( $query )
	{
		$this->qdescr = $query;
		$result = $this->query( $query );
		if( $result === false ) {
			return false;
		} else {
			$row = mysql_fetch_assoc( $result );
			mysql_free_result( $result );
			return $row;
		}
	}

	function getLastErrorMessage()
	{
		return $this->_dberror;
	}

	function getLastQuery()
	{
		return $this->qdescr;
	}

	function getLastInsertID()
	{
		return mysql_insert_id( $this->_dbhandle );
	}

	function escape_string( $string )
	{
		return mysql_real_escape_string( $string , $this->_dbhandle );
	}
}
