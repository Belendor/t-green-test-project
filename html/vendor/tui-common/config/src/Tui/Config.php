<?php

Namespace Tui;

use Tui\Config\ZendConfig;

class Config
{
	protected $_config;
	protected $_applicationName;

	private function __construct( array $config, $applicationName )
	{
		$this->_setConfig( $config );
		$this->_setApplicationName( $applicationName );
	}

	/**
	 * Get the configuration directory
	 *
	 * @return string
	 **/
	private static function _getConfigDirectory()
	{
		if( isset( $_SERVER[ 'PHP_CONFIG_PATH' ] ) ){
			return $_SERVER[ 'PHP_CONFIG_PATH' ];
		}

		if( isset( $_SERVER[ 'CONFIG_PATH' ] ) ){
			return $_SERVER[ 'CONFIG_PATH' ];
		}

		throw new \InvalidArgumentException( 'Configuration endpoint was not specified!', 503 );
	}

	/**
	 * Set the application name
	 *
	 * @param string $applicationName
	 */
	private function _setApplicationName( $applicationName )
	{
		$this->_applicationName = $applicationName;
	}

	/**
	 * Get the application name
	 *
	 * @return string
	 */
	private function _getApplicationName()
	{
		return $this->_applicationName;
	}
	/**
	 * Get the configuration for an application
	 *
	 * @param string $applicationName
	 *
	 * @return \Tui\Config
	 */
	public static function getConfig( $applicationName )
	{
		static $config = array();

		if( !is_string( $applicationName ) ){
			throw new \InvalidArgumentException( 'Application name is not a string.', 401 );
		}

		if( !isset( $config[ $applicationName ] ) ){
			$path = self::_validatepath( $applicationName );
			$configurationArray           = parse_ini_file( $path );

			$config[ $applicationName ]   = new self( $configurationArray, $applicationName );
		}

		return $config[ $applicationName ];
	}

	/**
	 * Get the configuration for a ZF1 application
	 *
	 * @param string $applicationName
	 *
	 * @param null   $section
	 *
	 * @return ZendConfig
	 */
	public static function getZf1Config( $applicationName, $section = null )
	{
		if( !is_string( $applicationName ) ){
			throw new \InvalidArgumentException( 'Application name is not a string.', 401 );
		}

		if( null !== $section && !is_string( $section ) ){
			throw new \InvalidArgumentException( 'Section must be a string' );
		}
		static $config = array();

		if( !isset( $config[ $applicationName ] ) ){
			$path                         = self::_validatepath( $applicationName );
			$config[ $applicationName ]   = new \Tui\Config\Ini( $path, $section );
		}

		return $config[ $applicationName ];
	}

	/**
	 * Validate if there's a config for given file
	 *
	 * @param $applicationName
	 *
	 * @return string path to file
	 */
	protected static function _validatepath( $applicationName )
	{
		if( stristr( $applicationName, '/' ) || stristr(  $applicationName, '\\' )) {
			throw new \InvalidArgumentException( 'Application name contains illegal characters ( `/` or `\\` ).', 402);
		}

		$path = self::_getConfigDirectory() .'/' . $applicationName .'.ini';

		if( ! file_exists( $path ) ){
			throw new \InvalidArgumentException( 'Configuration for application ' . $applicationName .' was not found.' , 400 );
		}

		if( ! is_readable( $path ) ){
			throw new \InvalidArgumentException( 'Configuration file is not readable.', 600 );
		}

		return $path;
	}


	/**
	 * Set the configuration settings
	 *
	 * @param array $config
	 */
	private function _setConfig( array $config )
	{
		$this->_config = $config;
	}

	/**
	 * Get all the configuration values
	 *
	 * @return array
	 */
	public function getValues()
	{
		return $this->_config;
	}

	/**
	 * Get the configuration value
	 *
	 * @param $value
	 *
	 * @return mixed
	 */
	public function getValue( $value )
	{
		if( ! is_string( $value ) ){
			throw new \InvalidArgumentException( 'Config key must be a string', 501 );
		}

		if( ! isset( $this->_config[ $value ] ) ){
			throw new \InvalidArgumentException( 'Config key ' . $value . ' does not exist for ' . $this->_getApplicationName() . '.', 500 );
		}

		return $this->_config[ $value ];
	}
}
